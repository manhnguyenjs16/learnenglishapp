package com.example.myapplication.topic;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.note.NoteActivity;
import com.example.myapplication.note.NoteAdapter;

import java.util.ArrayList;

public class DetailedItem extends AppCompatActivity {
    TextView textView_engName,textView_vieName,textView_pronounce,textView_exampleEn,textView_exampleVi;
    CheckBox checkBox_star,checkBox_speaker;
    int itemNumber;
    String fileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_item);
        textView_engName = findViewById(R.id.textView_engName);
        textView_vieName = findViewById(R.id.textView_vieName);
        textView_pronounce = findViewById(R.id.textView_pronounce);
        textView_exampleEn = findViewById(R.id.textView_exampleEn);
        textView_exampleVi = findViewById(R.id.textView_exampleVi);
        checkBox_speaker = findViewById(R.id.checkBox_speaker);
        checkBox_star = findViewById(R.id.checkBox_star);

        Intent intent = getIntent();
        itemNumber = intent.getIntExtra("ItemNumber",0);
        fileName = intent.getStringExtra("FileName");
        ArrayList<Item> arrayList = (ArrayList<Item>) ItemActivity.listItem.clone();

        String title = ItemActivity.listItem.get(itemNumber).getEngName().split("\\(")[0];
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setBackgroundDrawable((getDrawable(R.drawable.background)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textView_engName.setText(arrayList.get(itemNumber).getEngName());
        textView_vieName.setText(arrayList.get(itemNumber).getVieName());
        textView_pronounce.setText(arrayList.get(itemNumber).getPronoun());
        textView_exampleEn.setText(arrayList.get(itemNumber).getExampleEn());
        textView_exampleVi.setText(arrayList.get(itemNumber).getExampleVi());

        //Load note
        for (int index = 0; index < NoteActivity.listNote.size(); index++) {
            Item itemNote = NoteActivity.listNote.get(index);
            if (itemNote.getEngName().equals(ItemActivity.listItem.get(itemNumber).getEngName())) {
                checkBox_star.setChecked(true);
            }
        }
    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkBox_star:
                if (checked) {
                    NoteActivity.listNote.add(ItemActivity.listItem.get(itemNumber));
                } else {
                    for (int i = 0; i < NoteActivity.listNote.size(); i++) {
                        Item itemNote = NoteActivity.listNote.get(i);
                        if (itemNote.getEngName().equals(ItemActivity.listItem.get(itemNumber).getEngName())) {
                            NoteActivity.listNote.remove(itemNote);
                            break;
                        }
                    }
                }
                //overwrite listNote to file
                NoteAdapter.loadDataToFile(DetailedItem.this);
                // Remove the meat
                break;
            case R.id.checkBox_speaker:
                if (checked) {
                    ItemAdapter.textToSpeech(DetailedItem.this, textView_engName);
                    checkBox_speaker.setChecked(false);
                }
                break;
            // TODO: Veggie sandwich
        }
    }

    //Handle click backIcon
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:break;
        }
        return super.onOptionsItemSelected(item);
    }
}