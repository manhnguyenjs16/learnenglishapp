package com.example.myapplication.topic;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.note.NoteActivity;
import com.example.myapplication.note.NoteAdapter;

import java.util.List;
import java.util.Locale;

public class ItemAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Item> itemList;

    public ItemAdapter(Context context, int layout, List<Item> itemList) {
        this.context = context;
        this.layout = layout;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout,null);

        TextView engWord = view.findViewById(R.id.textviewEngWord);
        TextView vieName = view.findViewById(R.id.textviewVie);
        CheckBox note = view.findViewById(R.id.checkboxNote);
        CheckBox speak = view.findViewById(R.id.checkboxSpeaker);

        Item item = itemList.get(i);
        engWord.setText(item.getEngName());
        vieName.setText(item.getVieName());
        note.setFocusable(false);
        note.setFocusableInTouchMode(false);
        speak.setFocusable(false);
        speak.setFocusableInTouchMode(false);

        // Speaker
        speak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                textToSpeech(context, engWord);
                speak.setChecked(false);
            }
        });

        //Load note
        for (int index = 0; index < NoteActivity.listNote.size(); index++) {
            Item itemNote = NoteActivity.listNote.get(index);
            if (itemNote.getEngName().equals(item.getEngName())) {
                note.setChecked(true);
            }
        }

        //Add, remove note
        note.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    NoteActivity.listNote.add(item);
                } else {
                    for (int i = 0; i < NoteActivity.listNote.size(); i++) {
                        Item itemNote = NoteActivity.listNote.get(i);
                        if (itemNote.getEngName().equals(item.getEngName())) {
                            NoteActivity.listNote.remove(itemNote);
                            break;
                        }
                    }
                }

                //overwrite listNote to file
                NoteAdapter.loadDataToFile(context);
            }
        });
        return view;
    }

    //Text to Speech
    static TextToSpeech tts ;
    static float speed = 1;
    public static void textToSpeech(Context context, TextView engName) {
        tts = new TextToSpeech(context.getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.US);
                    tts.setSpeechRate(speed);
                    tts.speak(engName.getText().toString().split("\\(")[0], TextToSpeech.QUEUE_FLUSH, null, "");
                }
            }
        });
    }
}
