package com.example.myapplication.topic;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopicActivity extends AppCompatActivity {
    Map<String, List<String>> topicGroup;
    List<String> topicList;
    List<String> topicItemList;
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Topic");
        actionBar.setBackgroundDrawable((getDrawable(R.drawable.background)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_topic);

        //list topic
        topicList = new ArrayList<>();
        String[] topicArray = {"Tự nhiên", "Con người", "Các mối quan hệ", "Sự vật xung quanh", "Cuộc sống thường ngày",
                "Công việc", "Nghệ thuật", "Truyền thông", "Liên lạc, Tin tức", "Các từ chỉ trạng thái, mức độ"};
        topicList = Arrays.asList(topicArray);
        createTopicGroup();

        expandableListView = findViewById(R.id.listView_topic);
        expandableListAdapter = new TopicAdapter(this, topicList, topicGroup);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setIndicatorBoundsRelative(800,1050);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int lastPos = -1;
            @Override
            public void onGroupExpand(int curPos) {
                if(lastPos != -1 && curPos != lastPos){
                    expandableListView.collapseGroup(lastPos);
                }
                lastPos = curPos;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Intent intentItem = new Intent(TopicActivity.this, ItemActivity.class);
                String topicName = null;
                String[] natural = {"Animals", "Plants", "Fruits", "Vegetables", "Landscape", "Weather", "Environment", "Colors"};
                String[] people = {"Appearance", "Body", "Personality", "Emotions and Feelings"};
                String[] relationships = {"Family", "Relationships"};
                String[] thingsAround = {"Clothing", "Foods and Drinks", "Houses and Buildings", "Sports",
                        "Transportation", "Kitchen", "Living room", "Bedroom", "Bathroom", "City"};
                String[] life = {"Education", "Health", "Hobbies", "Shopping", "Traveling", "Dates and time", "Language", "Holiday"};
                String[] work = {"Company", "Jobs", "Working life"};
                String[] art = {"Arts"};
                String[] communication = {"Computer and the internet"};
                String[] telephoneLetter = {"TVs and Newspaper"};
                String[] adverb = {"Measurement", "Speed", "Frequency", "Degree", "Places", "Others"};
                switch (i){
                    case 0:topicName = natural[i1] ;
                        break;
                    case 1:topicName = people[i1] ;
                        break;
                    case 2:topicName = relationships[i1] ;
                        break;
                    case 3:topicName = thingsAround[i1] ;
                        break;
                    case 4:topicName = life[i1] ;
                        break;
                    case 5:topicName = work[i1] ;
                        break;
                    case 6:topicName = art[i1] ;
                        break;
                    case 7:topicName = communication[i1] ;
                        break;
                    case 8:topicName = telephoneLetter[i1] ;
                        break;
                    case 9:topicName = adverb[i1] ;
                        break;
                    default:break;
                }
                intentItem.putExtra("filename", topicName);
                startActivity(intentItem);
                return true;
            }
        });
    }
    private void loadTopicItemList(String[] itemTopicList) {
        topicItemList = new ArrayList<>();
        for (String item : itemTopicList) {
            topicItemList.add(item);
        }
    }

    private void createTopicGroup() {
        String[] natural = {"Động vật", "Cây cối", "Trái cây", "Rau củ", "Phong cảnh", "Thời tiết", "Môi trường", "Màu sắc"};
        String[] people = {"Ngoại hình", "Cơ thể", "Tính cách", "Cảm xúc và cảm giác"};
        String[] relationships = {"Gia đình", "Các mối quan hệ"};
        String[] thingsAround = {"Quần áo", "Đồ ăn và thức uống", "Nhà cửa", "Thể thao", "Giao thông",
                "Phòng bếp", "Phòng khách", "Phòng ngủ", "Phòng tắm", "Thành phố"};
        String[] life = {"Giáo dục", "Sức khoẻ", "Sở thích", "Mua sắm", "Du lịch", "Ngày và giờ", "Ngôn ngữ", "Ngày lễ tết"};
        String[] work = {"Công ty", "Công việc", "Đời sống công sở"};
        String[] art = {"Nghệ thuật"};
        String[] communication = {"Máy tính và mạng internet"};
        String[] telephoneLetter = {"Điện thoại, thư tín, truyền hình, báo chí"};
        String[] adverb = {"Đo lường", "Tốc độ", "Tần suất", "Mức độ", "Vị trí", "Các từ chỉ sự tăng-giảm"};

        topicGroup = new HashMap<String, List<String>>();
        for (String topic : topicList) {
            if (topic.equals("Tự nhiên")) {
                loadTopicItemList(natural);
            } else if (topic.equals("Con người")) {
                loadTopicItemList(people);
            } else if (topic.equals("Các mối quan hệ")) {
                loadTopicItemList(relationships);
            } else if (topic.equals("Sự vật xung quanh")) {
                loadTopicItemList(thingsAround);
            } else if (topic.equals("Cuộc sống thường ngày")) {
                loadTopicItemList(life);
            } else if (topic.equals("Công việc")) {
                loadTopicItemList(work);
            } else if (topic.equals("Nghệ thuật")) {
                loadTopicItemList(art);
            } else if (topic.equals("Truyền thông")) {
                loadTopicItemList(communication);
            } else if (topic.equals("Liên lạc, Tin tức")) {
                loadTopicItemList(telephoneLetter);
            } else if (topic.equals("Các từ chỉ trạng thái, mức độ")) {
                loadTopicItemList(adverb);
            }
            topicGroup.put(topic, topicItemList);
        }
    }

    //click back
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:break;
        }
        return super.onOptionsItemSelected(item);
    }
}