package com.example.myapplication.topic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class ItemActivity extends AppCompatActivity {
    ListView listView_item;
    ItemAdapter adapter;
    String filename;
    ImageView imgViewTopic;
    public static ArrayList<Item> listItem = new ArrayList<Item>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        imgViewTopic = findViewById(R.id.imageViewItem);

        Intent intent = getIntent();
        filename = intent.getStringExtra("filename");

        //set image view
        String lowerStr = filename.toLowerCase();
        String[] splitStr = lowerStr.split("\\ ");
        Context context = getApplicationContext();
        int id = context.getResources().getIdentifier("drawable/" + splitStr[0], null, context.getPackageName());
        imgViewTopic.setImageResource(id);


        createItemData();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(filename);
        actionBar.setBackgroundDrawable((getDrawable(R.drawable.background)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView_item = findViewById(R.id.listView_item);

        adapter = new ItemAdapter(this, R.layout.item, listItem);
        listView_item.setAdapter(adapter);
        listView_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent_detailedItem = new Intent(ItemActivity.this, DetailedItem.class);
                intent_detailedItem.putExtra("ItemNumber", i);
                startActivity(intent_detailedItem);
            }
        });

    }
    public void createItemData() {
        ArrayList<Item> arrayList = new ArrayList<Item>();
        try {
            InputStream inputStream = getAssets().open(filename+".txt");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            String text = new String(buffer);
            Scanner sc = new Scanner(text);
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] data = line.split("\t");
                Item item = new Item(data[0], data[1], data[2], data[3], data[4]);
                arrayList.add(item);
            }
            listItem = (ArrayList<Item>) arrayList.clone();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //click backIcon
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        adapter = new ItemAdapter(this, R.layout.item, listItem);
        listView_item.setAdapter(adapter);
        super.onResume();
    }
}