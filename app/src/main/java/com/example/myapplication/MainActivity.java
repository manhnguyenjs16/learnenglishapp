package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.example.myapplication.Reminder.AlarmReceiver;
import com.example.myapplication.Training.TrainingActivity;
import com.example.myapplication.Training.TrainingFragment;
import com.example.myapplication.Translates.Translate;
import com.example.myapplication.note.NoteActivity;
import com.example.myapplication.topic.TopicActivity;
import com.example.myapplication.topic.Item;
import com.google.android.material.tabs.TabLayout;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private CardView cardView_training;
    private CardView cardView_reminder;
    private CardView cardView_topic;
    private CardView cardView_note;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        cardView_training = findViewById(R.id.cardView_Training);
        cardView_reminder = findViewById(R.id.cardView_Reminder);
        cardView_topic = findViewById(R.id.cardView_Topic);
        cardView_note = findViewById(R.id.cardView_Note);
        CardView translate = findViewById(R.id.cardView_translate);

        translate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, Translate.class);
                startActivity(i);
            }
        });

        cardView_training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, TrainingActivity.class);
                startActivity(i);

            }
        });

        cardView_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createReminder();
                }

            }
        });

        cardView_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,TopicActivity.class);
                startActivity(intent);
            }
        });

        createNote();
        cardView_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NoteActivity.class);
                startActivity(intent);
            }
        });


    }

    private void createReminder() {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int y = i, m = i1, d = i2;
                TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {

                    Calendar calendar = Calendar.getInstance();

                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        calendar.set(y, m, d, i, i1 );
                        long reminderTimeInMillis = calendar.getTimeInMillis();
//                        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
                        createNotification(reminderTimeInMillis);
                    }

                }, hour, minute, true);

                calendar.set(i, i1, i2);
                timePickerDialog.show();


            }
        },year, month, day );

        datePickerDialog.show();
    }

    private void createNotification(long time) {

        createNotificationChanel();
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent);




    }
    public void createNote(){
        //load data from file
        NoteActivity.listNote.clear();
        try {
            FileInputStream fis = openFileInput("fileNote.txt");
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            String line;
            String data[];
            while ((line = bufferedReader.readLine()) != null) {
                data = line.split("\t");
                NoteActivity.listNote.add(new Item(data[0], data[1], data[2], data[3], data[4]));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Item item = new Item("Example (Noun)", "Ví dụ", "/ig´za:mp(ə)l/", "We have some examples.", "Chúng tôi có một số ví dụ.");
        if (NoteActivity.listNote.isEmpty()) {
            NoteActivity.listNote.add(item);
        }
        for (Item item1 : NoteActivity.listNote) {
            NoteActivity.listEngName.add(item1.getEngName());
        }
    }

    private void createNotificationChanel() {
        NotificationChannel notificationChannel = new NotificationChannel("channel1", "LearnEnglish!!!", NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setDescription("Time to watch movie!!");
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);

    }


}