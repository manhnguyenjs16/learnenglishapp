package com.example.myapplication.Training;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvQuestion;
    private TextView tvContentQuestion;
    private TextView tvAnswer1, tvAnswer2, tvAnswer3, tvAnswer4;
    ImageView btn_back;

    private List<Question>  mListQuestion;
    private Question mQuestion;
    private Integer currentQuestion = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        getSupportActionBar().hide();

        tvContentQuestion = findViewById(R.id.tv_content_question);
        tvAnswer1 = findViewById(R.id.tv_answer1);
        tvAnswer2 = findViewById(R.id.tv_answer2);
        tvAnswer3 = findViewById(R.id.tv_answer3);
        tvAnswer4 = findViewById(R.id.tv_answer4);
        btn_back = findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent intent = new Intent(QuizActivity.this, TrainingActivity.class);
                startActivity(intent);
            }
        });




        mListQuestion = getListQuestion();

        setDataQuestion(mListQuestion.get(currentQuestion));
    }

    private void setDataQuestion(Question question) {

        mQuestion = question;

        tvAnswer1.setBackgroundResource(R.drawable.bg_blue_corner_10);
        tvAnswer2.setBackgroundResource(R.drawable.bg_blue_corner_10);
        tvAnswer3.setBackgroundResource(R.drawable.bg_blue_corner_10);
        tvAnswer4.setBackgroundResource(R.drawable.bg_blue_corner_10);

        tvContentQuestion.setText(question.getContent());
        tvAnswer1.setText(question.getListAnswer().get(0).getContent());
        tvAnswer2.setText(question.getListAnswer().get(1).getContent());
        tvAnswer3.setText(question.getListAnswer().get(2).getContent());
        tvAnswer4.setText(question.getListAnswer().get(3).getContent());

        tvAnswer1.setOnClickListener(this);
        tvAnswer2.setOnClickListener(this);
        tvAnswer3.setOnClickListener(this);
        tvAnswer4.setOnClickListener(this);
    }

    private List<Question> getListQuestion() {
        List<Question> list = new ArrayList<>();

        List<Answer> answerList1 = new ArrayList<>();
        answerList1.add(new Answer("Con gà", false));
        answerList1.add(new Answer("Con cá", false));
        answerList1.add(new Answer("Con bò", false));
        answerList1.add(new Answer("Con chim", true));

        List<Answer> answerList2 = new ArrayList<>();
        answerList2.add(new Answer("Bụi cây", false));
        answerList2.add(new Answer("Mọc lên, lớn lên, phát triển", true));
        answerList2.add(new Answer("Chồi, búp cây", false));
        answerList2.add(new Answer("Lá cây", false));

        list.add(new Question("Bird", answerList1));
        list.add(new Question("Grow", answerList2));
        return list;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_answer1:
                tvAnswer1.setBackgroundResource(R.drawable.bg_orange_corner_30);
                checkAnswer(tvAnswer1, mQuestion, mQuestion.getListAnswer().get(0));
                break;

            case R.id.tv_answer2:
                tvAnswer2.setBackgroundResource(R.drawable.bg_orange_corner_30);
                checkAnswer(tvAnswer2, mQuestion, mQuestion.getListAnswer().get(1));
                break;

            case R.id.tv_answer3:
                tvAnswer3.setBackgroundResource(R.drawable.bg_orange_corner_30);
                checkAnswer(tvAnswer3, mQuestion, mQuestion.getListAnswer().get(2));
                break;

            case R.id.tv_answer4:
                tvAnswer4.setBackgroundResource(R.drawable.bg_orange_corner_30);
                checkAnswer(tvAnswer4, mQuestion, mQuestion.getListAnswer().get(3));
                break;
        }

    }

    private void checkAnswer(TextView tvAnswer, Question mQuestion, Answer answer) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (answer.isCorrect()) {
                    tvAnswer.setBackgroundResource(R.drawable.bg_green_corner_30);
                    nextQuestion();
                } else  {
                    tvAnswer.setBackgroundResource(R.drawable.bg_red_corner_30);
                    showAnswerCorrect(mQuestion);
                    gameOver();
                }
            }
        }, 1000);
    }

    private void gameOver() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showDialog("Game Over");
            }
        }, 1000);


    }

    private void showAnswerCorrect(Question question) {

        if (question.getListAnswer().get(0).isCorrect()) {


        }
    }

    private void nextQuestion() {
        if (currentQuestion == mListQuestion.size() - 1) {
            showDialog("You win");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    TrainingFragment fragment = new TrainingFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.main, fragment);
                    transaction.commit();
                }
            }, 2000);


        } else {
            currentQuestion++;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    setDataQuestion(mListQuestion.get(currentQuestion));
                }
            }, 1000);


        }
    }

    private void showDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                currentQuestion = 0;
                setDataQuestion(mListQuestion.get(currentQuestion));
                dialogInterface.dismiss();

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}