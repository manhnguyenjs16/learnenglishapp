package com.example.myapplication.Training;

import java.util.List;

public class Question {
    private String content;
    private List<Answer> listAnswer;

    public Question(String content, List<Answer> listAnswer) {
        this.content = content;
        this.listAnswer = listAnswer;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Answer> getListAnswer() {
        return listAnswer;
    }

    public void setListAnswer(List<Answer> listAnswer) {
        this.listAnswer = listAnswer;
    }
}
