package com.example.myapplication.Training;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

import java.util.Random;

public class WordGameActivity extends AppCompatActivity {


    private int presCounter = 0;
    private int maxPresCounter = 0;
    private String[] key1s = {"B", "e", "a", "r"};
    private String textAnswer1 = "Bear";
    private String[] key2s = {"A", "O", "B"};
    private String textAnswer2 = "DOG";
    ImageView btn_back;
    TextView textScreen, textQuestion, textTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_game);
        getSupportActionBar().hide();
        btn_back = findViewById(R.id.btn_back);



        key1s = shuffleArray(key1s);

        for (String key : key1s) {
            addView(((LinearLayout) findViewById(R.id.layoutParent)), key, ((EditText) findViewById(R.id.editText)), "Con gấu");
        }

        maxPresCounter = key1s.length;

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WordGameActivity.this, TrainingActivity.class);
                startActivity(intent);
            }
        });
    }

    private String[] shuffleArray(String[] ar) {
        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            String a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        return ar;
    }

    private void addView(LinearLayout viewParent, final String text, final EditText editText, String textVN) {
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        linearLayoutParams.rightMargin = 30;

        final TextView textView = new TextView(this);

        textView.setLayoutParams(linearLayoutParams);
        textView.setBackground(this.getResources().getDrawable(R.drawable.bg_orange_corner_30));
        textView.setTextColor(this.getResources().getColor(R.color.black));
        textView.setGravity(Gravity.CENTER);
        textView.setText(text);
        textView.setClickable(true);
        textView.setFocusable(true);
        textView.setTextSize(32);


        textQuestion = (TextView) findViewById(R.id.textQuestion);
        textScreen = (TextView) findViewById(R.id.textScreen);
        textTitle = (TextView) findViewById(R.id.textTitle);
        textQuestion.setText(textVN);



        textView.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if(presCounter < maxPresCounter) {
                    if (presCounter == 0)
                        editText.setText("");

                    editText.setText(editText.getText().toString() + text);
                    textView.animate().alpha(0).setDuration(300);
                    presCounter++;

                    if (presCounter == maxPresCounter)
                        doValidate();
                }
            }
        });


        viewParent.addView(textView);


    }

    private void doValidate() {
        presCounter = 0;

        EditText editText = findViewById(R.id.editText);
        LinearLayout linearLayout = findViewById(R.id.layoutParent);

        if(editText.getText().toString().equals(textAnswer1)) {
            Toast.makeText(WordGameActivity.this, "Correct", Toast.LENGTH_SHORT).show();
            editText.setText("");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    key1s[0] = "B";
                    key1s[1] = "r";
                    key1s[2] = "i";
                    key1s[3] = "d";
                    textAnswer1 = "Bird";
                    key1s = shuffleArray(key1s);
                    maxPresCounter = key1s.length;
                    linearLayout.removeAllViews();
                    for (String key : key1s) {
                        addView(((LinearLayout) findViewById(R.id.layoutParent)), key, ((EditText) findViewById(R.id.editText)), "Con chim");
                    }
                }
            }, 1000);
        } else {
            Toast.makeText(WordGameActivity.this, "Wrong", Toast.LENGTH_SHORT).show();
            editText.setText("");
        }

        key1s = shuffleArray(key1s);
        linearLayout.removeAllViews();
        for (String key : key1s) {
            addView(linearLayout, key, editText, "Con gấu");
        }

    }


}