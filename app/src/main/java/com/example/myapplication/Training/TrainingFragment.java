package com.example.myapplication.Training;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

public class TrainingFragment extends Fragment {

    Button btn_wordgame;
    Button btn_quiz;
    ImageView   btn_back;
    CardView cardView_wg, cardView_quiz;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.training_fragment, container, false);
        cardView_wg = view.findViewById(R.id.cardView_wg);
        cardView_quiz = view.findViewById(R.id.cardView_quiz);
        btn_back = view.findViewById(R.id.btn_back);

        cardView_quiz.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent intent = new Intent(getActivity(),QuizActivity.class);
                 startActivity(intent);
             }
         });

        cardView_wg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),WordGameActivity.class);
                startActivity(intent);
            }
        });



        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });





        return view;
    }
}
