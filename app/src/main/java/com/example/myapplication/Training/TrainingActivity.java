package com.example.myapplication.Training;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;

public class TrainingActivity extends AppCompatActivity {

    ImageView btn_back;
    CardView cardView_wg, cardView_quiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);
        getSupportActionBar().hide();

        cardView_wg = findViewById(R.id.cardView_wg);
        cardView_quiz = findViewById(R.id.cardView_quiz);
        btn_back = findViewById(R.id.btn_back);

        cardView_quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrainingActivity.this,QuizActivity.class);
                startActivity(intent);
            }
        });

        cardView_wg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrainingActivity.this,WordGameActivity.class);
                startActivity(intent);
            }
        });



        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrainingActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}