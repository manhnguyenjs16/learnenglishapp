package com.example.myapplication.note;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.topic.Item;
import com.example.myapplication.topic.ItemAdapter;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

public class NoteAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Item> itemList;

    public NoteAdapter(Context context, int layout, List<Item> itemList) {
        this.context = context;
        this.layout = layout;
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout,null);
        }

        TextView engWord = view.findViewById(R.id.textviewEngWord);
        TextView vieName = view.findViewById(R.id.textviewVie);
        CheckBox note = view.findViewById(R.id.checkboxNote);
        CheckBox speak = view.findViewById(R.id.checkboxSpeaker);

        // Gán giá trị
        Item item = itemList.get(i);
        engWord.setText(item.getEngName());
        vieName.setText(item.getVieName());
        note.setFocusable(false);
        note.setFocusableInTouchMode(false);
        speak.setFocusable(false);
        speak.setFocusableInTouchMode(false);

        //Load note from file to App
        for (int index = 0; index < NoteActivity.listNote.size(); index++) {
            note.setChecked(true);
        }

        // Text to Speech
        speak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                ItemAdapter.textToSpeech(context, engWord);
                speak.setChecked(false);
            }
        });

        //Add, remove note
        note.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    for (int i = 0; i < itemList.size(); i++) {
                        Item itemNote = itemList.get(i);
                        if (itemNote.getEngName().equals(item.getEngName())) {
                            itemList.remove(itemNote);
                            notifyDataSetChanged();
                            break;
                        }
                    }
                }

                //overwrite listNote to file
                loadDataToFile(context);
            }
        });
        return view;
    }

    //Overwrite data from App to fileNote
    public static void loadDataToFile(Context context) {
        try {
            FileOutputStream fos = context.openFileOutput("fileNote.txt", Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            for (Item noteItem: NoteActivity.listNote) {
                osw.write(noteItem.getEngName() + "\t" + noteItem.getVieName() + "\t"
                        + noteItem.getPronoun() + "\t" + noteItem.getExampleEn() + "\t"
                        + noteItem.getExampleVi() + "\n");
            }
            osw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
