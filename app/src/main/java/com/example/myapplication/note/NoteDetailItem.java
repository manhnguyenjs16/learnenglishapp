package com.example.myapplication.note;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.R;
import com.example.myapplication.topic.Item;
import com.example.myapplication.topic.ItemAdapter;

import java.util.ArrayList;

public class NoteDetailItem extends AppCompatActivity {
    TextView textView_engName,textView_vieName,textView_pronounce,textView_exampleEn,textView_exampleVi;
    CheckBox checkBox_note,checkBox_speaker;
    int itemNum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_item);

        Intent intent = getIntent();
        itemNum = intent.getIntExtra("ItemNumberNote", 0);
        String title = NoteActivity.listNote.get(itemNum).getEngName().split("\\(")[0];
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setBackgroundDrawable((getDrawable(R.drawable.background)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<Item> arrayList = (ArrayList<Item>) NoteActivity.listNote.clone();
        textView_engName = findViewById(R.id.textView_engName);
        textView_vieName = findViewById(R.id.textView_vieName);
        textView_pronounce = findViewById(R.id.textView_pronounce);
        textView_exampleEn = findViewById(R.id.textView_exampleEn);
        textView_exampleVi = findViewById(R.id.textView_exampleVi);
        checkBox_speaker = findViewById(R.id.checkBox_speaker);
        checkBox_note = findViewById(R.id.checkBox_star);

        textView_engName.setText(arrayList.get(itemNum).getEngName());
        textView_vieName.setText(arrayList.get(itemNum).getVieName());
        textView_pronounce.setText(arrayList.get(itemNum).getPronoun());
        textView_exampleEn.setText(arrayList.get(itemNum).getExampleEn());
        textView_exampleVi.setText(arrayList.get(itemNum).getExampleVi());
        checkBox_note.setChecked(true);
    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkBox_star:
                if (!checked) {
                    NoteActivity.listNote.remove(itemNum);
                }
                //overwrite listNote to file
                NoteAdapter.loadDataToFile(NoteDetailItem.this);
                break;
            case R.id.checkBox_speaker:
                if (checked) {
                    ItemAdapter.textToSpeech(NoteDetailItem.this, textView_engName);
                    checkBox_speaker.setChecked(false);
                }
                break;
        }
    }

    // click backIcon
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                Intent intent = new Intent(NoteDetailItem.this, NoteActivity.class);
                startActivity(intent);
                return true;
            default:break;
        }
        return super.onOptionsItemSelected(item);
    }
}