package com.example.myapplication.Translates;

import static android.Manifest.permission.CAMERA;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslator;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslatorOptions;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.normal.TedPermission;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Translate extends AppCompatActivity {

    private TextInputEditText sourceEdit;
    private ImageView mic, swap, cam;
    private Button translate;
    private TextView result, from, to;
    private ImageButton addButton;
    private int fromLanguageCode = FirebaseTranslateLanguage.EN;
    private int toLanguageCode = FirebaseTranslateLanguage.VI;
    private static final int REQUEST_MIC_CAPTURE = 1;
    private String fromText = "eng";
    private Boolean flags = false;
    Boolean micCheck = false;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translate);
        getSupportActionBar().hide();
        actionBar();
        sourceEdit = findViewById(R.id.Source);
        mic = findViewById(R.id.Mic);
        swap = findViewById(R.id.Swap);
        cam = findViewById(R.id.Cam);
        translate = findViewById(R.id.Translate);
        result = findViewById(R.id.Result);
        from = findViewById(R.id.From);
        to = findViewById(R.id.To);

        translate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(sourceEdit.getWindowToken(), 0);
                sourceEdit.clearFocus();
                result.setText("");
                if (sourceEdit.getText().toString().isEmpty()) {
                    Toast.makeText(Translate.this, "Vui lòng nhập từ/văn bản cần dịch", Toast.LENGTH_SHORT).show();
                } else {
                    translateText(fromLanguageCode, toLanguageCode, sourceEdit.getText().toString());
                }
            }
        });

        swap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tempLanguage = from.getText().toString();
                from.setText(to.getText().toString());
                to.setText(tempLanguage);
                int tempLanguageCode = fromLanguageCode;
                fromLanguageCode = toLanguageCode;
                toLanguageCode = tempLanguageCode;
                fromText = fromText.equals("eng") ? "vie" : "eng";
            }
        });

        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                isUsingMic = true;
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                if(fromLanguageCode == FirebaseTranslateLanguage.EN){
                    i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
                }else{
                    i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "vi-VN");
                }
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, "Nói vào mic để chuyển thành từ/văn bản");
                try{
                    startActivityForResult(i, REQUEST_MIC_CAPTURE);
                }catch(Exception e){
                    e.printStackTrace();
                    Toast.makeText(Translate.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, 1001);
                    }
                    else {
                        pickImageFromGallery();
                    }
                }
                else {
                    pickImageFromGallery();
                }

            }

        });

    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1000);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sourceEdit.setText("How are you");

            }
        }, 1000);

    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1001: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImageFromGallery();
                } else {
                    Toast.makeText(this, "Permission denied...", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    private void translateText(int fromLanguageCode, int toLanguageCode, String source){
        if(sourceEdit.getText().toString().isEmpty()) {
            Toast.makeText(Translate.this, "Vui lòng nhập từ/văn bản cần dịch", Toast.LENGTH_SHORT).show();
            return;
        }
        result.setText("Đang tải dữ liệu...");
        FirebaseTranslatorOptions options = new FirebaseTranslatorOptions.Builder()
                .setSourceLanguage(fromLanguageCode)
                .setTargetLanguage(toLanguageCode)
                .build();
        FirebaseTranslator translator = FirebaseNaturalLanguage.getInstance().getTranslator(options);
        FirebaseModelDownloadConditions conditions = new FirebaseModelDownloadConditions.Builder().build();

        translator.downloadModelIfNeeded(conditions).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                result.setText("Đang dịch...");
                translator.translate(source).addOnSuccessListener(new OnSuccessListener<String>() {
                    @Override
                    public void onSuccess(String s) {
                        result.setText(s);
                        flags = true;
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Translate.this, "Xảy ra lỗi khi dịch", Toast.LENGTH_SHORT).show();
                        flags = false;
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(Translate.this, "Xảy ra lỗi khi tải dữ liệu", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void actionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Translate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_MIC_CAPTURE) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                sourceEdit.setText(result.get(0));
            }
        }
    }


}